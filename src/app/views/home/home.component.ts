import { Component, OnInit } from '@angular/core';
import { ForecastNow } from 'src/app/shared/model/ForecastNow.model';
import { FutureForecastItem } from 'src/app/shared/model/FutureForecastItem.model';
import { ForecastService } from 'src/app/shared/service/forecast.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
  value             : string = ''
  date              : string = ''
  pathToIcon        : string = ''
  forecast          : Array<FutureForecastItem>
  forecastNow       : ForecastNow
  notFound          : boolean = false

  // array to map wheater x icon-wheater 
  indexedArrayIconsForecast: {[key: string]: string} = {
    storm:         '../../../assets/img/forecast/storm.png',       // tempestade (chuva forte)
    rain:          '../../../assets/img/forecast/rain.png',        // chuva (chuviscos)
    cloudy:        '../../../assets/img/forecast/cloudy.png',      // parcialmente nublado
    cloud:         '../../../assets/img/forecast/cloud.png',       // nublado / noite
    cloudly_day:   '../../../assets/img/forecast/cloudyday.png',   // nublado
    clear_day:     '../../../assets/img/forecast/sunny.png',       // enrolarado
    clear_night:   '../../../assets/img/forecast/clear-night.png', // céu limpo a noite
    cloudly_night: '../../../assets/img/forecast/cloudy-night.png' // céu nublado a noite
  }

  // array to map the day of week
  indexedArrayWeekday: {[key: string]: string} = {
    Seg: 'Segunda-feira',
    Ter: 'Terça-feira',
    Qua: 'Quarta-feira',
    Qui: 'Quinta-feira',
    Sex: 'Sexta-feira',
    Sáb: 'Sábado',
    Dom: 'Domingo'
  }

  constructor(
    public forecastService: ForecastService
  ) {}

  ngOnInit() : void{
    navigator.permissions && navigator.permissions.query({name: 'geolocation'})
    .then(function(PermissionStatus) {
      if (PermissionStatus.state == 'granted') {
        document.getElementById('labelAdvert').innerHTML = "Caso queira saber a previsão de outro local, <br> basta digitar o nome da cidade no campo abaixo!";
        let btnLocation = document.getElementById('buttonEnableLocation')
        btnLocation.click();
        btnLocation.style.display = 'none';
      } else if (PermissionStatus.state == 'prompt') {
        console.log('prompt - not yet grated or denied')
      } else {
        document.getElementById('labelAdvert').innerHTML = "Ops, parece que estamos bloqueados para acessar sua localização &nbsp; :( <br> Caso não quiser habilitá-la, basta digitar o nome da cidade no campo abaixo.";
      }
    })
    
  }

  public enableLocation(): void{
    if(!navigator.geolocation){
      console.log('location is not supported');
    }
    navigator.geolocation.getCurrentPosition((position) => {
      console.log(position.coords);
      let latitude  = position.coords.latitude;
      let longitude = position.coords.longitude;
      this.getWeatherForecastByLocation(latitude, longitude);
    });
  }

  getWeatherForecastByLocation(latitude, longitude){
    this.forecastService.getWeatherByCoordinates(latitude, longitude).subscribe(data => {
      this.forecastNow = data.results
      this.value    = data.results.city;
      this.forecast = data.results.forecast;
      this.date     = data.results.date;
    });
  }

  getWeatherForecastByCity(){
    this.forecastService.getWeatherByCity(this.value).subscribe(data => {
      if(data.by == 'default'){
        this.notFound = true;
        return
      }
      this.notFound = false;
      this.forecastNow = data.results
      this.value    = data.results.city;
      this.forecast = data.results.forecast;
      this.date     = data.results.date;
    });
  }
}

import { ForecastNow } from "./ForecastNow.model";

export class OverviewForecast{
    by: string;
    valid_key: boolean;
    results: ForecastNow;
    execution_time: number;
    from_cache: true;
}
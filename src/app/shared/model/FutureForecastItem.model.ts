export class FutureForecastItem{
    date: string;
    weekday: string;
    max: number;
    min: number;
    description: string;
    condition: string;
}
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { OverviewForecast } from '../model/OverviewForecast.model';

@Injectable({
  providedIn: 'root'
})
export class ForecastService {

  private readonly apiUrl = 'http://127.0.0.1:5000';

  httpOptions = {
    headers: new HttpHeaders({
      'Contet-Type': 'application/json'
    })
  };
  
  constructor(
    private httpClient: HttpClient
  ) { }
  
  public getWeatherByCoordinates(latitude: string, longitude: string): Observable<OverviewForecast>{
    let params = new HttpParams().set('latitude', latitude).set('longitude', longitude);
    return this.httpClient.get<OverviewForecast>(this.apiUrl + '/forecast-by-coords', { params });
  }

  public getWeatherByCity(city: string): Observable<OverviewForecast>{
    let params = new HttpParams().set('city', city);
    return this.httpClient.get<OverviewForecast>(this.apiUrl + '/forecast-by-city', { params });
  }

}

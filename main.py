# from fastapi import Depends, FastAPI
# from fastapi.middleware.cors import CORSMiddleware
# from fastapi_cache import caches, close_caches
# from fastapi_cache.backends.redis import CACHE_KEY, RedisCacheBackend
# import http.client
# import json

# app = FastAPI()
# conn = http.client.HTTPSConnection("api.hgbrasil.com")

# # permitindo requests localhost (corrige erro CORS)
# origins = [
#     "http://localhost:4200",
# ]
# app.add_middleware(
#     CORSMiddleware,
#     allow_origins=origins,
#     allow_credentials=True,
#     allow_methods=["*"],
#     allow_headers=["*"],
# )

# @app.get('/forecast-by-coords')
# async def index(latitude: str, longitude: str):
#     conn.request("GET", "/weather?key=1e326da7&lat=" + latitude + "&lon=" + longitude + "&user_ip=remote")
#     res  = conn.getresponse()
#     data = res.read()
#     data = json.loads(data)
#     return data

# @app.get('/forecast-by-city')
# async def index(city: str):
#     conn.request("GET", "/weather?key=1e326da7&city_name=" + city)
#     res  = conn.getresponse()
#     data = res.read()
#     data = json.loads(data)
#     return data
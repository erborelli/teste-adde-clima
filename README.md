## Executando a API

Execute `flask run` para inicializar a API.

## Rodando o Projeto

Execute no terminal `ng serve` para subir o projeto. E navegue até `http://localhost:4200/`.

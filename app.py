from flask import Flask, jsonify, request, make_response
from urllib.request import urlopen
from flask_cors import CORS, cross_origin
from urllib.request import urlopen                                                                                                                                                            
from urllib.parse import quote 
from flask_caching import Cache

app   = Flask(__name__)
# cors
cors  = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'
# cache
cache = Cache(config={'CACHE_TYPE': 'simple'})
cache.init_app(app)

# base url api
apiUrl = 'http://api.hgbrasil.com'

@app.route('/forecast-by-coords', methods=['GET'])
@cross_origin()
@cache.cached(timeout=900, query_string=True)
def forecastByCoords():
    # get params
    latitude  = request.args['latitude']
    longitude = request.args['longitude']
    # make request
    with urlopen(apiUrl + "/weather?key=37748e3a&lat=" + latitude + "&lon=" + longitude + "&user_ip=remote") as r:
        response = r.read()

    return response


@app.route('/forecast-by-city', methods=['GET'])
@cross_origin()
@cache.cached(timeout=900, query_string=True)
def forecastByCity():

    # get params
    cityName  = request.args['city']
    endpoint = apiUrl + "/weather?key=37748e3a&city_name=" + quote(cityName)
    
    # return quote(endpoint)
    # make request
    with urlopen(endpoint) as r:
        response = r.read()

    return response


if __name__ == '__main__':
    app.run(debug=True)